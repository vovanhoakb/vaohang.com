<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the page header div.
 *
 * @package Hestia
 * @since Hestia 1.0
 */
$wrapper_div_classes = 'wrapper ';
if ( is_single() ) {
	$wrapper_div_classes .= join( ' ', get_post_class() );
}

$layout               = apply_filters( 'hestia_header_layout', get_theme_mod( 'hestia_header_layout', 'default' ) );
$disabled_frontpage   = get_theme_mod( 'disable_frontpage_sections', false );
$wrapper_div_classes .=
	(
		( is_front_page() && ! is_page_template() && ! is_home() && false === (bool) $disabled_frontpage ) ||
		( class_exists( 'WooCommerce', false ) && ( is_product() || is_product_category() ) ) ||
		( is_archive() && ( class_exists( 'WooCommerce', false ) && ! is_shop() ) )
	) ? '' : ' ' . $layout . ' ';

$header_class = '';
$hide_top_bar = get_theme_mod( 'hestia_top_bar_hide', true );
if ( (bool) $hide_top_bar === false ) {
	$header_class .= 'header-with-topbar';
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">

<head>
	<meta charset='<?php bloginfo( 'charset' ); ?>'>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/wp-content/uploads/images/ico.ico"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div class="<?php echo esc_attr( $wrapper_div_classes ); ?>">
		<header class="header <?php echo esc_attr( $header_class ); ?>">
			<div class="wp-login-register">
				<div class="container">
					<ul>
						<li><a href="https://www.f8bet0.com/?a=213784" target="_blank">Đăng nhập</a></li>
						<li><a href="https://www.f8bet0.com/?a=213784" target="_blank">Đăng ký</a></li>
					</ul>
				</div>
			</div>
			<div class="container header-logo">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6 logo-web">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<?php 
								$image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' );
								$logo = esc_url($image[0]);
							?>
							<img src="<?php echo $logo;?>" alt="Logo">
						</a>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 header-web">
						<a href="https://www.f8bet0.com/?a=213784" target="_blank">
							<img src="/wp-content/uploads/images/bn.gif" alt="Banner">
						</a>
						<a id="hamburger"><span></span></a>
					</div>
				</div>
				<div class="wraper-menu">
					<?php do_action( 'hestia_do_header' ); ?>
					<span class="hide-menu"></span>
				</div>
			</div>
			<?php
				hestia_before_header_trigger();
				do_action( 'hestia_do_top_bar' );
				do_action( 'hestia_do_header' );
				hestia_after_header_trigger();
			?>
		</header>
		<div class="left_img">
			<!-- https://www.f8betaa.com/signup -->
			<a href="https://www.f8bet0.com/?a=213784" target="_blank"><img src="/wp-content/uploads/images/left.gif" alt="Banner left"></a>
		</div>
		<div class="right_img">
			<a href="https://www.f8bet0.com/?a=213784" target="_blank"><img src="/wp-content/uploads/images/right.gif" alt="Banner right"></a>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<script type="text/javascript">
			$("#hamburger").on('click',function(){
                $(".wraper-menu").show(300);
                // $('.header').toggleClass('mobile');
                $('body').css('overflow','hidden');
            })
            $('.hide-menu').on('click',function(){
            	$('.wraper-menu').hide(300);
                $('body').css('overflow','inherit');
            })

            $(window).scroll(function () {
                if ($(window).scrollTop() > 150 ) {
                    if($('.left_img').hasClass('active')){
                    
                    }else{
                        $('.left_img').addClass('active')
                    }
                    if($('.right_img').hasClass('active')){
                    
                    }else{
                        $('.right_img').addClass('active')
                    }
                }else{
                    if($('.left_img').hasClass('active')){
                        $('.left_img').removeClass('active')
                    }
                    if($('.right_img').hasClass('active')){
                        $('.right_img').removeClass('active')
                    }
                }
            })
		</script>