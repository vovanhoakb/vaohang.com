
			<!-- <?php do_action( 'hestia_do_footer' ); ?> -->
			<div class="footer-end">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6 left-footer">
							<div class="lf-content-ft">
								<h2>VAOHANG.COM</h2>
								<div class="text-lf">
									VAOHANG là trang web trực tiếp bóng đá miễn phí với chất lượng cao và là kênh xem bóng đá trực tuyến được yêu thích nhất Việt Nam. Nơi mà tất cả các giải đấu bóng đá hàng đầu trong cho đến ngoài nước đều được trực tiếp đầy đủ. Giúp bạn xem được trận đấu mình thích với trải nghiệm cao nhất. Chính vì thế, nếu có nhu cầu xem bất kỳ trận đấu nào, bạn hãy truy cập vào đây để lấy được link xem bóng đá uy tín nhất nhé.
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 right-footer">
							<div class="wp-itemview">
								<?php 
									$args = array( 'posts_per_page' => 3 );
									$lastposts = get_posts( $args );
									foreach ( $lastposts as $post ) :
									  setup_postdata( $post ); 
								?>
									<div class="item-post">
										<div class="img-post">
											<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
												<?php the_post_thumbnail( 'hestia-blog' ); ?>
											</a>
										</div>
										<div class="name-post">
											<a href="<?php the_permalink(); ?>">
												<?php echo the_title();?>
											</a>
										</div>
									</div>
								<?php 
									endforeach; 
									wp_reset_postdata();
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="end_footer">
				<div class="container">
					<ul>
						<!-- <?php
							global $wpdb;
						    $limit 		= 5;
						    $offset 	= 0;
						    $sql = "SELECT * FROM wp_terms ORDER BY `term_id` ASC LIMIT 5";
						    $data = $wpdb->get_results( $wpdb->prepare($sql, $limit, $offset), ARRAY_A);
						    foreach ($data as $key => $value) {
						    	$category_link = get_category_link( $value['term_id'] );
						?>
							<li class="">
								<a href="<?php echo esc_url($category_link); ?>"><span><?php echo $value['name'];?></span></a>
							</li>
						<?php } ?> -->
						<li>
							<a href="https://vaohang.com/lich-thi-dau-bong-da/"><span>LỊCH THI ĐẤU BÓNG ĐÁ</span></a>
						</li>
						<li>
							<a href="https://vaohang.com/ket-qua-bong-da/"><span>KẾT QUẢ BÓNG ĐÁ</span></a>
						</li>
						<li>
							<a href="https://vaohang.com/nhan-dinh-bong-da/"><span>NHẬN ĐỊNH BÓNG ĐÁ</span></a>
						</li>
						<li>
							<a href="https://vaohang.com/soi-keo-bong-da/"><span>SOI KÈO BÓNG ĐÁ</span></a>
						</li>
						<li>
							<a href="https://vaohang5.tv/highlight/"><span>Highlight</span></a>
						</li>
						<li>
							<a href="https://vaohang5.tv/bxh/"><span>BXH</span></a>
						</li>
						<li>
							<a href="https://vaohang5.tv/top-nha-cai/"><span>Top nhà cái</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php wp_footer(); ?>
</body>
</html>