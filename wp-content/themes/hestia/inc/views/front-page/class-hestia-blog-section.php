<?php
/**
 * The Blog Section
 *
 * @package Hestia
 */

/**
 * Class Hestia_Blog_Section
 */
class Hestia_Blog_Section extends Hestia_Abstract_Main {
	/**
	 * Initialize Blog Section
	 */
	public function init() {
		$this->hook_section();
	}

	/**
	 * Hook section in/
	 */
	private function hook_section() {
		$section_priority = apply_filters( 'hestia_section_priority', 60, 'hestia_blog' );
		add_action( 'hestia_sections', array( $this, 'do_section' ), absint( $section_priority ), 2 );
		add_action( 'hestia_do_blog_section', array( $this, 'render_section' ) );
	}

	/**
	 * Executes the hook on which the content is rendered.
	 */
	public function do_section() {
		do_action( 'hestia_do_blog_section', false );
	}

	/**
	 * Blog section content.
	 */
	public function render_section( $is_shortcode = false ) {

		/**
		 * Don't show section if Disable section is checked.
		 * Show it if it's called as a shortcode.
		 */
		$hide_section  = get_theme_mod( 'hestia_blog_hide', false );
		$section_style = '';
		if ( $is_shortcode === false && (bool) $hide_section === true ) {
			if ( is_customize_preview() ) {
				$section_style = 'style="display: none"';
			} else {
				return;
			}
		}

		/**
		 * Gather data to display the section.
		 */
		if ( current_user_can( 'edit_theme_options' ) ) {
			/* translators: 1 - link to customizer setting. 2 - 'customizer' */
			$hestia_blog_subtitle = get_theme_mod( 'hestia_blog_subtitle', sprintf( __( 'Change this subtitle in the %s.', 'hestia' ), sprintf( '<a href="%1$s" class="default-link">%2$s</a>', esc_url( admin_url( 'customize.php?autofocus&#91;control&#93;=hestia_blog_subtitle' ) ), __( 'Customizer', 'hestia' ) ) ) );
		} else {
			$hestia_blog_subtitle = get_theme_mod( 'hestia_blog_subtitle' );
		}
		$hestia_blog_title = get_theme_mod( 'hestia_blog_title', __( 'Blog', 'hestia' ) );
		if ( $is_shortcode ) {
			$hestia_blog_title    = '';
			$hestia_blog_subtitle = '';
		}

		/**
		 * In case this function is called as shortcode, we remove the container and we add 'is-shortcode' class.
		 */
		$wrapper_class   = $is_shortcode === true ? 'is-shortcode' : '';
		$container_class = $is_shortcode === true ? '' : 'container';

		$html_allowed_strings = array(
			$hestia_blog_title,
			$hestia_blog_subtitle,
		);
		maybe_trigger_fa_loading( $html_allowed_strings );

		hestia_before_blog_section_trigger(); ?>
		<section class="root-home">
			<div class="container">
				<div class="content-home">
					<?php 
						$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=1&cat=33'); 
						global $wp_query; $wp_query->in_the_loop = true;
						while ($getposts->have_posts()) : $getposts->the_post();
							echo the_content();
						endwhile; wp_reset_postdata(); 
					?>
				</div>
				<div class="category_name">
					<ul>
						<li>Tin mới nhất</li>
						<?php
							// global $wpdb;
						 //    $limit 		= 5;
						 //    $offset 	= 0;
						 //    $sql = "SELECT * FROM wp_terms ORDER BY `term_id` ASC LIMIT 5";
						 //    $data = $wpdb->get_results( $wpdb->prepare($sql, $limit, $offset), ARRAY_A);
						 //    foreach ($data as $key => $value) {}
						 //    	$category_link = get_category_link( $value['term_id'] );
						?>
							<!-- <li class="" style="display: none !important;">
								<a href="<?php echo esc_url($category_link); ?>"><span><?php echo $value['name'];?></span></a>
							</li> -->
						<li class="" style="display: none !important;">
							<a href="https://vaohang5.tv/highlight/"><span>Highlight</span></a>
						</li>
						<li class="" style="display: none !important;">
							<a href="https://vaohang5.tv/bxh/"><span>BXH</span></a>
						</li>
						<li class="" style="display: none !important;">
							<a href="https://vaohang5.tv/top-nha-cai/"><span>Top nhà cái</span></a>
						</li>
					</ul>
				</div>
				<div class="row row-highlight">
					<div class="col-md-6 col-xs-6 col-sm-6 left-new-edit">
						<div class="wp-newlf">
							<?php 
								$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=1&cat=3'); 
								global $wp_query; $wp_query->in_the_loop = true;
								while ($getposts->have_posts()) : $getposts->the_post();
							?>
								<div class="item-new-edit">
									<div class="img-new-edit img-c1">
										<a href="<?php the_permalink(); ?>">
											<?php echo get_the_post_thumbnail( get_the_id(), '', array( 'class' =>'thumnail') ); ?>
										</a>
										<div class="text-new-edit">
											<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											<p><?php the_time('d/m/Y');?></p>
										</div>
									</div>
								</div>
							<?php endwhile; wp_reset_postdata(); ?>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-6 left-new-edit">
						<div class="item-two-new">
							<?php 
								$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=2&cat=3'); 
								global $wp_query; $wp_query->in_the_loop = true;
								while ($getposts->have_posts()) : $getposts->the_post();
							?>
								<div class="item-new-edit item-two">
									<div class="img-new-edit height_css">
										<a href="<?php the_permalink(); ?>">
											<?php echo get_the_post_thumbnail( get_the_id(), '', array( 'class' =>'thumnail') ); ?>
										</a>
										<div class="text-new-edit">
											<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											<p><?php the_time('d/m/Y');?></p>
										</div>
									</div>
								</div>
							<?php endwhile; wp_reset_postdata(); ?>
						</div>
						<div class="item-down-new">
							<div class="row row-highlight">
								<?php 
									$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=4&cat=3'); 
									global $wp_query; $wp_query->in_the_loop = true;
									while ($getposts->have_posts()) : $getposts->the_post();
								?>
									<div class="col-md-6 col-xs-6 col-sm-6 wp-item-tw-edit">
										<div class="item-new-edit">
											<div class="img-new-edit">
												<a href="<?php the_permalink(); ?>">
													<?php echo get_the_post_thumbnail( get_the_id(), '', array( 'class' =>'thumnail') ); ?>
												</a>
												<div class="text-new-edit">
													<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
													<p><?php the_time('d/m/Y');?></p>
												</div>
											</div>
										</div>
									</div>
								<?php endwhile; wp_reset_postdata(); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9 col-sm-9 col-xs-12 left-home">
						<div class="banner-home">
							<img src="/wp-content/uploads/images/banner.png" alt="Banner">
						</div>
						<div class="wp-worldcup">
							<h2 class="title-home">Lịch thi đấu bóng đá</h2>
							<div class="row">
								<?php 
									$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=6&cat=1'); 
									global $wp_query; $wp_query->in_the_loop = true;
									while ($getposts->have_posts()) : $getposts->the_post();
								?>
									<div class="col-md-4 col-sm-4 col-xs-6 item-worldcup">
										<div class="wp-item-wc">
											<div class="img-wc">
												<a href="<?php the_permalink(); ?>">
													<?php echo get_the_post_thumbnail( get_the_id(), '', array( 'class' =>'thumnail') ); ?>
												</a>
											</div>
											<div class="text-wc">
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											</div>
										</div>
									</div>
								<?php endwhile; wp_reset_postdata(); ?>
							</div>
						</div>
						<div class="wp-soikeo">
							<h2 class="title-home">Soi kèo bóng đá</h2>
							<div class="row">
								<?php 
									$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=6&cat=9'); 
									global $wp_query; $wp_query->in_the_loop = true;
									while ($getposts->have_posts()) : $getposts->the_post();
								?>
									<div class="col-md-4 col-sm-4 col-xs-6 item-worldcup">
										<div class="wp-item-wc">
											<div class="img-wc">
												<a href="<?php the_permalink(); ?>">
													<?php echo get_the_post_thumbnail( get_the_id(), '', array( 'class' =>'thumnail') ); ?>
												</a>
											</div>
											<div class="text-wc">
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											</div>
										</div>
									</div>
								<?php endwhile; wp_reset_postdata(); ?>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 right-home">
						<div class="wp-right-home">
							<h3 class="tit-right">Soi kèo vip</h3>
							<div class="ul-social">
								<ul>
									<li class="li1"><a href="" target="_blank"><img src="/wp-content/uploads/images/fb.png" alt="Icon social"></a></li>
									<li class="li2"><a href="" target="_blank"><img src="/wp-content/uploads/images/tw.png" alt="Icon social"></a></li>
									<li class="li3"><a href="" target="_blank"><img src="/wp-content/uploads/images/tl.png" alt="Icon social"></a></li>
									<li class="li4"><a href="" target="_blank"><img src="/wp-content/uploads/images/yt.png" alt="Icon social"></a></li>
									<li class="li5"><a href="" target="_blank"><img src="/wp-content/uploads/images/pr.png" alt="Icon social"></a></li>
								</ul>
							</div>
							<div class="wp-chauau">
								<h2 class="tit-chauau">Nhận định bóng đá</h2>
								<div class="wp-new-chauau">
									<?php 
										$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=5&cat=4'); 
										global $wp_query; $wp_query->in_the_loop = true;
										while ($getposts->have_posts()) : $getposts->the_post();
									?>
										<div class="item-new-chauau">
											<div class="img-new-chauau">
												<a href="<?php the_permalink(); ?>">
													<?php echo get_the_post_thumbnail( get_the_id(), '', array( 'class' =>'thumnail') ); ?>
												</a>
											</div>
											<div class="text-new-chauau">
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											</div>
										</div>
									<?php endwhile; wp_reset_postdata(); ?>
								</div>
							</div>
							<div class="wp-highlight">
								<h2 class="tit-chauau">Highlight</h2>
								<div class="wp-new-chauau">
									<div class="row row-highlight">
										<?php 
											$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=6&cat=10'); 
											global $wp_query; $wp_query->in_the_loop = true;
											while ($getposts->have_posts()) : $getposts->the_post();
										?>
											<div class="col-md-6 col-xs-12 col-sm-6 item-highlight">
												<div class="item-new-highlight">
													<div class="img-new-highlight">
														<a href="<?php the_permalink(); ?>">
															<?php echo get_the_post_thumbnail( get_the_id(), 'thumbnail', array( 'class' =>'thumnail') ); ?>
														</a>
													</div>
													<div class="text-new-highlight">
														<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
													</div>
												</div>
											</div>
										<?php endwhile; wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
							<div class="wp-highlight" style="display: none;">
								<h2 class="tit-chauau">Kết quả bóng đá</h2>
								<div class="wp-new-chauau">
									<div class="row row-highlight">
										<?php 
											$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=6&cat=3'); 
											global $wp_query; $wp_query->in_the_loop = true;
											while ($getposts->have_posts()) : $getposts->the_post();
										?>
											<div class="col-md-6 col-xs-12 col-sm-6 item-highlight">
												<div class="item-new-highlight">
													<div class="img-new-highlight">
														<a href="<?php the_permalink(); ?>">
															<?php echo get_the_post_thumbnail( get_the_id(), 'thumbnail', array( 'class' =>'thumnail') ); ?>
														</a>
													</div>
													<div class="text-new-highlight">
														<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
													</div>
												</div>
											</div>
										<?php endwhile; wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php dynamic_sidebar('block-after-content'); ?>
		</section>
		<section style="display: none;" class="hestia-blogs <?php echo esc_attr( $wrapper_class ); ?>" id="blog"
			data-sorder="hestia_blog" <?php echo wp_kses_post( $section_style ); ?>>
			<?php
			hestia_before_blog_section_content_trigger();
			if ( $is_shortcode === false ) {
				hestia_display_customizer_shortcut( 'hestia_blog_hide', true );
			}
			?>
			<div class="<?php echo esc_attr( $container_class ); ?>">
				<?php
				hestia_top_blog_section_content_trigger();
				if ( $is_shortcode === false ) {
					?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center hestia-blogs-title-area">
							<?php
							hestia_display_customizer_shortcut( 'hestia_blog_title' );
							if ( ! empty( $hestia_blog_title ) || is_customize_preview() ) {
								echo '<h2 class="hestia-title">' . wp_kses_post( $hestia_blog_title ) . '</h2>';
							}
							if ( ! empty( $hestia_blog_subtitle ) || is_customize_preview() ) {
								echo '<h5 class="description">' . hestia_sanitize_string( $hestia_blog_subtitle ) . '</h5>';
							}
							?>
						</div>
					</div>
					<?php
				}
				?>
				<div class="hestia-blog-content">
				<?php
				$this->blog_content();
				?>
				</div>
				<?php hestia_bottom_blog_section_content_trigger(); ?>
			</div>
			<?php hestia_after_blog_section_content_trigger(); ?>
		</section>
		<?php
		hestia_after_blog_section_trigger();
	}

	/**
	 * Blog content/
	 */
	public function blog_content() {

		$hestia_blog_items      = get_theme_mod( 'hestia_blog_items', 3 );
		$args                   = array(
			'ignore_sticky_posts' => true,
		);
		$args['posts_per_page'] = ! empty( $hestia_blog_items ) ? absint( $hestia_blog_items ) : 3;

		$hestia_blog_categories = get_theme_mod( 'hestia_blog_categories' );

		if ( ! empty( $hestia_blog_categories[0] ) && sizeof( $hestia_blog_categories ) >= 1 ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => $hestia_blog_categories,
				),
			);
		}

		$loop = new WP_Query( $args );

		$allowed_html = array(
			'br'     => array(),
			'em'     => array(),
			'strong' => array(),
			'i'      => array(
				'class' => array(),
			),
			'span'   => array(),
		);

		if ( ! $loop->have_posts() ) {
			return;
		}
			$i = 1;
			echo '<div class="row" ' . hestia_add_animationation( 'fade-up' ) . '>';
		while ( $loop->have_posts() ) :
			$loop->the_post();
			?>
			<article class="col-xs-12 col-ms-10 col-ms-offset-1 col-sm-8 col-sm-offset-2 <?php echo esc_attr( apply_filters( 'hestia_blog_per_row_class', 'col-md-4' ) ); ?> hestia-blog-item">
				<div class="card card-plain card-blog">
					<?php if ( has_post_thumbnail() ) : ?>
							<div class="card-image">
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail( 'hestia-blog' ); ?>
								</a>
							</div>
						<?php endif; ?>
					<div class="content">
						<h6 class="category"><?php echo hestia_category(); ?></h6>
						<h4 class="card-title entry-title">
							<a class="blog-item-title-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
								<?php echo wp_kses( force_balance_tags( get_the_title() ), $allowed_html ); ?>
							</a>
						</h4>
						<p class="card-description"><?php echo wp_kses_post( get_the_excerpt() ); ?></p>
					</div>
				</div>
				</article>
				<?php
				if ( $i % apply_filters( 'hestia_blog_per_row_no', 3 ) === 0 ) {
					echo '</div><!-- /.row -->';
					echo '<div class="row" ' . hestia_add_animationation( 'fade-up' ) . '>';
				}
				$i++;
			endwhile;
			echo '</div>';

			wp_reset_postdata();
	}

}